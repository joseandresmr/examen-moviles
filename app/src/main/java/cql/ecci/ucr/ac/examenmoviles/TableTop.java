package cql.ecci.ucr.ac.examenmoviles;

import android.os.Parcel;
import android.os.Parcelable;

public class TableTop implements Parcelable
{
    public static final String key = "TableTop";

    private String id;
    private String name;
    private int year;
    private String publisher;
    private String country;
    private double latitude;
    private double longitude;
    private String description;
    private String noPlayers;
    private String ages;
    private String playingTime;
    private int icon;

    public TableTop(){}

    public TableTop(Parcel in)
    {
        this.id = in.readString();
        this.name = in.readString();
        this.year = in.readInt();
        this.publisher = in.readString();
        this.country = in.readString();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.description = in.readString();
        this.noPlayers = in.readString();
        this.ages = in.readString();
        this.playingTime = in.readString();
        this.icon = in.readInt();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNoPlayers() {
        return noPlayers;
    }

    public void setNoPlayers(String noPlayers) {
        this.noPlayers = noPlayers;
    }

    public String getAges() {
        return ages;
    }

    public void setAges(String ages) {
        this.ages = ages;
    }

    public String getPlayingTime() {
        return playingTime;
    }

    public void setPlayingTime(String playingTime) {
        this.playingTime = playingTime;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeInt(year);
        dest.writeString(publisher);
        dest.writeString(country);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(description);
        dest.writeString(noPlayers);
        dest.writeString(ages);
        dest.writeString(playingTime);
        dest.writeInt(icon);
    }

    public static final Creator<TableTop> CREATOR = new Creator<TableTop>() {
        @Override
        public TableTop createFromParcel(Parcel in) {
            return new TableTop(in);
        }
        @Override
        public TableTop[] newArray(int size) {
            return new TableTop[size];
        }
    };
}
