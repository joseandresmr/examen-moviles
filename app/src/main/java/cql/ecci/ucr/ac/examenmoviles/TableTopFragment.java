package cql.ecci.ucr.ac.examenmoviles;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TableTopFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TableTopFragment extends Fragment
{
    private static final String NAME_PARAM = "param1";

    // TODO: Rename and change types of parameters
    private TableTop tableTop;

    public TableTopFragment() {
        // Required empty public constructor
    }

    public static TableTopFragment newInstance(TableTop tableTopParam) {
        TableTopFragment fragment = new TableTopFragment();
        Bundle args = new Bundle();
        args.putParcelable(NAME_PARAM, tableTopParam);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            tableTop = getArguments().getParcelable(NAME_PARAM);
        }
    }

    public TableTop getTableTop()
    {
        return tableTop;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Must inflate before setting the texts
        View view = inflater.inflate(R.layout.fragment_table_top, container,false);

        TextView name = view.findViewById(R.id.table_top_name);
        name.setText(tableTop.getName());

        TextView description = view.findViewById(R.id.table_top_description);
        description.setText(tableTop.getDescription());

        TextView year = view.findViewById(R.id.table_top_year);
        year.setText(String.valueOf(tableTop.getYear()));

        TextView publisher = view.findViewById(R.id.table_top_publisher);
        publisher.setText(tableTop.getPublisher());

        TextView country = view.findViewById(R.id.table_top_country);
        country.setText(tableTop.getCountry());

        TextView noPlayers = view.findViewById(R.id.table_top_no_players);
        noPlayers.setText(tableTop.getNoPlayers());

        TextView ages = view.findViewById(R.id.table_top_ages);
        ages.setText(tableTop.getAges());

        TextView playingTime = view.findViewById(R.id.table_top_playing_time);
        playingTime.setText(tableTop.getPlayingTime());

        Button buttonMap = view.findViewById(R.id.buttonMap);
        buttonMap.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                goMap(tableTop.getLatitude(), tableTop.getLongitude());
            }
        });

        return view;
    }

    public void goMap(double latitude, double longitude)
    {
        if(getActivity() != null)
        {
            Intent intent = new Intent(getActivity().getApplicationContext(), MapsActivity.class);
            intent.putExtra("latitude", latitude);
            intent.putExtra("longitude", longitude);
            startActivity(intent);
        }

    }
}
