package cql.ecci.ucr.ac.examenmoviles;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{
    private DatabaseHelper db;
    private ArrayList<TableTopItem> list;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapterRV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DatabaseHelper(this);
        db.deleteTuples();
        insertInitialData();
        loadItems();
        setupRecyclerView();
    }

    public void setupRecyclerView()
    {
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.table_top_rv);


        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {

                    @Override
                    public void onItemClick(View view, int position) {
                        goDetails(list.get(position).getName());
                    }
                })
        );

        // Performance
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        adapterRV = new AdapterRV(db.loadTableTopsItems());
        recyclerView.setAdapter(adapterRV);
    }

    // Go to a specific table top
    public void goDetails(String name)
    {
        Intent intent = new Intent(this, DetailsActivity.class);

        TableTop tableTop = db.getTableTop(name);
        intent.putExtra(TableTop.key, tableTop);

        startActivity(intent);
    }

    // Get items to display. It will get only name, description and the icon.
    public void loadItems()
    {
        list = db.loadTableTopsItems();
    }

    // Insercion de las tuplas
    public void insertInitialData()
    {
        TableTop catan = new TableTop();
        catan.setName("Catan");
        catan.setId("TT001");
        catan.setYear(1995);
        catan.setPublisher("Kosmos");
        catan.setCountry("Germany");
        catan.setLatitude(48.774538);
        catan.setLongitude(49.188467);
        catan.setDescription("Picture yourself in the era of discoveries: " +
                "after a long voyage of great deprivation, " +
                "your ships have finally reached the coast of " +
                "an uncharted island. Its name shall be Catan! " +
                "But you are not the only discoverer. Other " +
                "fearless seafarers have also landed on the " +
                "shores of Catan: the race to settle the " +
                "island has begun!");
        catan.setNoPlayers("3-4");
        catan.setAges("10+");
        catan.setPlayingTime("1-2 hours");
        catan.setIcon(R.drawable.catan);


        TableTop monopoly = new TableTop();
        monopoly.setName("Monopoly");
        monopoly.setId("TT002");
        monopoly.setYear(1935);
        monopoly.setPublisher("Hasbro");
        monopoly.setCountry("United States");
        monopoly.setLatitude(41.883736);
        monopoly.setLongitude(-71.352259);
        monopoly.setDescription("The thrill of bankrupting an opponent, but it " +
                "pays to play nice, because fortunes could " +
                "change with the roll of the dice. Experience " +
                "the ups and downs by collecting property " +
                "colors sets to build houses, and maybe even " +
                "upgrading to a hotel!");
        monopoly.setNoPlayers("2-8");
        monopoly.setAges("8+");
        monopoly.setPlayingTime("20-180 minutes");
        monopoly.setIcon(R.drawable.monopoly);

        TableTop eldritch = new TableTop();
        eldritch.setName("Monopoly");
        eldritch.setId("TT003");
        eldritch.setYear(2013);
        eldritch.setPublisher("Fantasy Flight Games");
        eldritch.setCountry("United States");
        eldritch.setLatitude(45.015417);
        eldritch.setLongitude(-93.183995);
        eldritch.setDescription("An ancient evil is stirring. You are part of a team of unlikely " +
                "heroes engaged in an international struggle to stop the gathering darkness. " +
                "To do so, you’ll have to defeat foul monsters, travel to Other Worlds, and " +
                "solve obscure mysteries surrounding this unspeakable horror. The effort may " +
                "drain your sanity and cripple your body, but if you fail, the Ancient One will " +
                "awaken and rain doom upon the known world.");
        eldritch.setNoPlayers("1-8");
        eldritch.setAges("14+");
        eldritch.setPlayingTime("2-4 hours");
        eldritch.setIcon(R.drawable.eldritch);

        TableTop magic = new TableTop();
        magic.setName("Magic: the Gathering");
        magic.setId("TT004");
        magic.setYear(1993);
        magic.setPublisher("Hasbro");
        magic.setCountry("United States");
        magic.setLatitude(41.883736);
        magic.setLongitude(-71.352259);
        magic.setDescription("Magic: The Gathering is a collectible and digital " +
                "collectible card game created by Richard Garfield. Each game of Magic " +
                "represents a battle between wizards known as planeswalkers who cast spells, " +
                "use artifacts, and summon creatures as depicted on individual cards in order " +
                "to defeat their opponents, typically, but not always, by draining them of " +
                "their 20 starting life points in the standard format.");
        magic.setNoPlayers("2+");
        magic.setAges("13+");
        magic.setPlayingTime("Varies");
        magic.setIcon(R.drawable.mtg);

        TableTop hanabi = new TableTop();
        hanabi.setName("Hanabi");
        hanabi.setId("TT004");
        hanabi.setYear(2010);
        hanabi.setPublisher("Asmodee");
        hanabi.setCountry("France");
        hanabi.setLatitude(48.761629);
        hanabi.setLongitude(2.065296);
        hanabi.setDescription("Hanabi—named for the Japanese word for \"fireworks\"—is a " +
                "cooperative game in which players try to create the perfect fireworks show " +
                "by placing the cards on the table in the right order.");
        hanabi.setNoPlayers("2-5");
        hanabi.setAges("8+");
        hanabi.setPlayingTime("25 minutes");
        hanabi.setIcon(R.drawable.hanabi);

        db.insertTableTop(catan);
        db.insertTableTop(monopoly);
        db.insertTableTop(eldritch);
        db.insertTableTop(magic);
        db.insertTableTop(hanabi);

    }
}
