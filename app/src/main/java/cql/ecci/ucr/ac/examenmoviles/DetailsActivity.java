package cql.ecci.ucr.ac.examenmoviles;

import androidx.appcompat.app.AppCompatActivity;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        // Obtener objeto
        TableTop tableTop = getIntent().getParcelableExtra(TableTop.key);

        final TableTopFragment tableTopFragment = TableTopFragment.newInstance(tableTop);

        // Transaccion para inicializar y asignar el fragmento
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, tableTopFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }
}
