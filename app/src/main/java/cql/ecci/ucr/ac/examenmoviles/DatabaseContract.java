package cql.ecci.ucr.ac.examenmoviles;

import android.provider.BaseColumns;

public class DatabaseContract
{
    // Para asegurar que no se instancie la clase hacemos el constructor privado
    private DatabaseContract() {}

    // Definimos una clase interna que define las tablas y columnas
    // Implementa la interfaz BaseColumns para heredar campos estandar del marco de Android _ID
    public static class DataBaseEntry implements BaseColumns
    {
        // Clase Persona
        public static final String TABLE_NAME_TABLE_TOP = "TableTop";

        // Falta el Id, que se inserta al cambiar _ID, entonces no es necesario ponerlo

        public static final String COLUMN_NAME_NAME = "Name";

        public static final String COLUMN_NAME_YEAR = "Year";

        public static final String COLUMN_NAME_PUBLISHER = "Publisher";

        public static final String COLUMN_NAME_COUNTRY = "Country";

        public static final String COLUMN_NAME_LATITUDE = "Latitude";

        public static final String COLUMN_NAME_LONGITUDE = "Longitude";

        public static final String COLUMN_NAME_DESCRIPTION = "Description";

        public static final String COLUMN_NAME_NO_PLAYERS = "NoPlayers";

        public static final String COLUMN_NAME_AGES = "Ages";

        public static final String COLUMN_NAME_PLAYING_TIME = "PlayingTime";

        public static final String COLUMN_NAME_ICON = "Icon";
    }

    // Construir las tablas de la base de datos
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA_SEP = ",";

    // Creacion de la tabla TableTop
    public static final String SQL_CREATE_TABLE_TOP =
            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_TABLE_TOP
                    + " (" +
                    DataBaseEntry._ID + TEXT_TYPE + "PRIMARY KEY," +
                    DataBaseEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_YEAR + INTEGER_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_PUBLISHER + TEXT_TYPE +
                    COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_COUNTRY + TEXT_TYPE +
                    COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_LATITUDE + REAL_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_LONGITUDE + REAL_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE +
                    COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_NO_PLAYERS + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_AGES + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_PLAYING_TIME + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_ICON + INTEGER_TYPE
                    + " )";

    public static final String SQL_DELETE_TABLE_TOP =
            "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_TABLE_TOP;
}
