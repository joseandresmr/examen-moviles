package cql.ecci.ucr.ac.examenmoviles;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper
{
    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "TableTopDB.db";

    public DatabaseHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DatabaseContract.SQL_CREATE_TABLE_TOP);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        // Administracion de actualizaciones
        db.execSQL(DatabaseContract.SQL_DELETE_TABLE_TOP);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        onUpgrade(db, oldVersion, newVersion);
    }

    // SELECT * FROM
    public ArrayList<TableTopItem> loadTableTopsItems()
    {
        ArrayList<TableTopItem> result = new ArrayList();
        String query = "Select * FROM " + DatabaseContract.DataBaseEntry.TABLE_NAME_TABLE_TOP;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        TableTopItem item;

        while (cursor.moveToNext())
        {
            item = new TableTopItem();
            item.setName(cursor.getString(1));
            item.setDescription(cursor.getString(7));
            item.setIcon(cursor.getInt(11));
            result.add(item);
        }
        cursor.close();
        db.close();
        return result;
    }

    public TableTop getTableTop(String name)
    {
        TableTop result = new TableTop();
        String query = "Select * FROM " + DatabaseContract.DataBaseEntry.TABLE_NAME_TABLE_TOP
                + " WHERE Name=" + "'" + name + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst())
        {
            result.setId(cursor.getString(0));
            result.setName(cursor.getString(1));
            result.setYear(cursor.getInt(2));
            result.setPublisher(cursor.getString(3));
            result.setCountry(cursor.getString(4));
            result.setLatitude(cursor.getDouble(5));
            result.setLongitude(cursor.getDouble(6));
            result.setDescription(cursor.getString(7));
            result.setNoPlayers(cursor.getString(8));
            result.setAges(cursor.getString(9));
            result.setPlayingTime(cursor.getString(10));
            result.setIcon(cursor.getInt(11));
        }



        cursor.close();
        db.close();
        return result;
    }

    public void deleteTuples()
    {
        String query = "DELETE FROM " + DatabaseContract.DataBaseEntry.TABLE_NAME_TABLE_TOP;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }


    public long insertTableTop(TableTop tableTop)
    {
        // Obtiene la base de datos en modo escritura
        SQLiteDatabase db = this.getWritableDatabase();

        // Crear un mapa de valores donde las columnas son las llaves
        ContentValues values = new ContentValues();

        // Llave primaria
        values.put(
                DatabaseContract.DataBaseEntry._ID, tableTop.getId());

        values.put(
                DatabaseContract.DataBaseEntry.COLUMN_NAME_NAME, tableTop.getName());

        values.put(
                DatabaseContract.DataBaseEntry.COLUMN_NAME_YEAR, tableTop.getYear());

        values.put(
                DatabaseContract.DataBaseEntry.COLUMN_NAME_PUBLISHER, tableTop.getPublisher());

        values.put(
                DatabaseContract.DataBaseEntry.COLUMN_NAME_COUNTRY, tableTop.getCountry());

        values.put(
                DatabaseContract.DataBaseEntry.COLUMN_NAME_LATITUDE, tableTop.getLatitude());

        values.put(
                DatabaseContract.DataBaseEntry.COLUMN_NAME_LONGITUDE, tableTop.getLongitude());

        values.put(
                DatabaseContract.DataBaseEntry.COLUMN_NAME_DESCRIPTION, tableTop.getDescription());

        values.put(
                DatabaseContract.DataBaseEntry.COLUMN_NAME_NO_PLAYERS, tableTop.getNoPlayers());

        values.put(
                DatabaseContract.DataBaseEntry.COLUMN_NAME_AGES, tableTop.getAges());

        values.put(
                DatabaseContract.DataBaseEntry.COLUMN_NAME_PLAYING_TIME, tableTop.getPlayingTime());

        values.put(
                DatabaseContract.DataBaseEntry.COLUMN_NAME_ICON, tableTop.getIcon());

        // Insertar la nueva fila
        return db.insert(DatabaseContract.DataBaseEntry.TABLE_NAME_TABLE_TOP, null, values);
    }
}
